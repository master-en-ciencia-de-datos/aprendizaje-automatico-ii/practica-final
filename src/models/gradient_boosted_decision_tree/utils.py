import copy

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import accuracy_score
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from src.data.make_dataset import get_processed, get_X_y
from src.visualization.plot_mean_std import plot_mean_std


def get_data(rm_outlayers, normalize, scale, best_params, param_grid,
             random_split=0.2, file_path=None):
    df = get_processed(rm_outlayers, normalize, scale,
                       dumies=["neighbourhood",
                               "neighbourhood_group"],
                       drop_columns=None)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(24, 6))

    OPTIONS = ["max_depth", "n_estimators"]

    for option in OPTIONS:

        temp_best_params = copy.copy(best_params)
        temp_best_params.pop(option)
        mean_train_score, mean_val_score = [], []
        std_train_score, std_val_score = [], []

        for atribute in tqdm(param_grid[option]):
            X_train, X_test, y_train, y_test = get_X_y(df, random_split)
            # 10 measures in order to get a mean and std
            temp_train_score, temp_val_score = [], []
            for i in range(10):
                gbdt_clf = GradientBoostingClassifier(**{**{option: atribute},
                                                         **temp_best_params})
                gbdt_clf.fit(X_train, y_train)
                temp_train_score.append(
                    accuracy_score(y_train, gbdt_clf.predict(X_train)))
                temp_val_score.append(
                    accuracy_score(y_test, gbdt_clf.predict(X_test)))
            mean_train_score.append(np.mean(temp_train_score))
            mean_val_score.append(np.mean(temp_val_score))
            std_train_score.append(np.std(temp_train_score))
            std_val_score.append(np.std(temp_val_score))

        if option == "max_depth":
            plotinfo = {"xvals": param_grid[option],
                        "xlabel": "Profundidad máxima",
                        "title": "Precisión en función de la profundidad máxima"}
            _ax = plot_mean_std(mean_train_score, mean_val_score,
                                std_train_score, std_val_score, plotinfo,
                                ax=ax1)
        else:
            plotinfo = {"xvals": param_grid[option],
                        "xlabel": "Número de árboles",
                        "title": "Precisión en función del número de árboles"}
            _ax = plot_mean_std(mean_train_score, mean_val_score,
                                std_train_score, std_val_score, plotinfo,
                                ax=ax2)

    plt.savefig(file_path)
