"""
Testing the GBDT model. The idea is:
    1. Train and test with 10 folds of CVscore
    2. save results in a .json
"""
import json
import os
import numpy as np

from sklearn.ensemble import GradientBoostingClassifier

from src.visualization.confussion_matrix import get_acc_f1
from tqdm import tqdm

list_acc_score = []
list_f1_score = [[], [], []]
list_train_time = []

N_ITER = 10

for iteracion in tqdm(range(N_ITER), desc="GBDT", leave=False):
    model = GradientBoostingClassifier(learning_rate=0.2,max_depth=1,
                                       min_samples_leaf=10,n_estimators=200,
                                       loss="deviance")

    acc_score, _f1_score, train_time = get_acc_f1(model,
                                      rm_outlayers=True, normalize=False,
                                      scale=False,)
    list_acc_score.append(acc_score)
    list_f1_score[0].append(_f1_score[0])
    list_f1_score[1].append(_f1_score[1])
    list_f1_score[2].append(_f1_score[2])
    list_train_time.append(train_time)

json_to_write = {
    "acc_score": np.mean(list_acc_score),
    "f1_score": {
        "micro": np.mean(list_f1_score[0]),
        "macro": np.mean(list_f1_score[1]),
        "weighted": np.mean(list_f1_score[2]),
    },
    "train_time": np.mean(list_train_time)
}

with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "data",
                       "results", "{}.json".format("gbdt")), 'w') as outfile:
    json.dump(json_to_write, outfile)