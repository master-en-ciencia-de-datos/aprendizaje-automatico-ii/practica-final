"""
Hyperparameter tuning for Gradient Boosted Decision Tree. The idea is:
  For both using bootstrap or not:
    1. Search the best hiperparameters iterating over the options of
    get_processed().
    2. Select the best feature engineer technique and follow with it
    3. Plot train_val score using n_estimators and max_depth
    4. Plot a confussión matrix over the validation set
    5. Plot loss during training
"""
import os

from sklearn.ensemble import GradientBoostingClassifier

from src.models.gradient_boosted_decision_tree.best_feature_engineer import look_for_best_feature_engineer_options
from src.models.gradient_boosted_decision_tree.utils import get_data
from src.visualization.confussion_matrix import get_confussion_matrix
from src.models.params import gbdt_params as gbdt_param_grid
from src.models.utils import write_results
"""
CV = 10

# 1. Best feature engineering
print("-"*10 + " 1. Looking for best params and feat eng " + "-"*10)
(loss_option, rm_outlayers, normalize, scale), best_params = look_for_best_feature_engineer_options(gbdt_param_grid, CV)

# 2. The chosen ones
print("-"*10 + " 2. The result " + "-"*10)
print("Loss option: {}\nRemove outlayers: {}\nNormalize: {}\nScale: {}\nBest "
      "params: {} "
      .format(loss_option, rm_outlayers, normalize, scale, best_params))

# 3. Plot using n_estimators, max_depth and the CV score
print("-"*10 + " 3. Plotting " + "-"*10)
param_grid = {'max_depth': [1, 2, 5, 10, 20],
              'n_estimators': [5, 10, 50, 100, 200]
              }

best_params["loss"] = loss_option
print(best_params)

"""
rm_outlayers=True
normalize=False
scale=False
param_grid = {'max_depth': [1, 2, 5, 10, 20],
              'n_estimators': [5, 10, 50, 100, 200]
              }
best_params={
    "learning_rate": 0.2,
    "max_depth": 1,
    "min_samples_leaf": 10,
    "n_estimators": 200,
    "loss": "deviance"
}
get_data(rm_outlayers, normalize, scale, best_params, param_grid,
         random_split=0.2, file_path=os.path.join(os.path.abspath(
                                                  os.path.dirname(__file__)),
                                                  "images", "gbdt-train"))

"""
# 4. Plot confussion matrix
print("-"*10 + " 4. Confussion matrix " + "-"*10)

gradient_boosted = GradientBoostingClassifier(**best_params)
acc_score, _f1_score = get_confussion_matrix(gradient_boosted, rm_outlayers,
                                             normalize, scale,
                                             random_split=0.2,
                                             title="Gradient Boosting",
                                             train_score=True,
                                             model_path=os.path.abspath(
                                                 os.path.dirname(__file__)))

json_to_write = {
    "rm_outlayers": rm_outlayers,
    "normalize": normalize,
    "scale": scale,
    "best_params": best_params,
    "acc_score": acc_score,
    "f1_score": {
        "micro": _f1_score[0],
        "macro": _f1_score[1],
        "weighted": _f1_score[2],
    }
}
write_results(json_to_write, os.path.join(os.path.abspath(
    os.path.dirname(__file__)),
    "data", "{}.json".format("gradient_boosted_decision_tree")))
"""