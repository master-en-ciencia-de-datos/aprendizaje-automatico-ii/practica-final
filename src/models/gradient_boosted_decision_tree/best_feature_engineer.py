from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV
import numpy as np
from tqdm import tqdm

from src.data.make_dataset import get_X_y, get_processed

LOSS_OPTIONS = ['deviance', 'exponential']
FEATURE_ENGINEERING_OPTIONS = {"rm_outlayers": [True, False],
                               "normalize": [True, False],
                               "scale": [True, False]
                               }


def look_for_best_feature_engineer_options(gbdt_param_grid, cv):
    scores = []
    options = []
    params = []
    pbar = tqdm(total=16)
    for loss_option in LOSS_OPTIONS:
        for rm_outlayers in FEATURE_ENGINEERING_OPTIONS["rm_outlayers"]:
            for normalize in FEATURE_ENGINEERING_OPTIONS["normalize"]:
                for scale in FEATURE_ENGINEERING_OPTIONS["normalize"]:
                    df = get_processed(rm_outlayers, normalize, scale,
                                       dumies=["neighbourhood",
                                               "neighbourhood_group"],
                                       drop_columns=None)

                    X_train, _, y_train, _ = get_X_y(df, random_split=0)
                    gbdt_clf = GradientBoostingClassifier()
                    gbdt_clf_grid_search = GridSearchCV(
                        estimator=gbdt_clf,
                        param_grid=gbdt_param_grid,
                        cv=cv, n_jobs=3, verbose=1)

                    gbdt_clf_grid_search.fit(X_train, y_train)

                    options.append((loss_option, rm_outlayers, normalize, scale))
                    scores.append(gbdt_clf_grid_search.best_score_)
                    params.append(gbdt_clf_grid_search.best_params_)
                    pbar.update(1)
    pbar.close()

    return options[np.argmax(scores)], params[np.argmax(scores)]
