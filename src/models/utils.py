import json
import os

def write_results(json_to_write, file_path):


    with open(file_path, 'w') \
            as outfile:
        json.dump(json_to_write, outfile)