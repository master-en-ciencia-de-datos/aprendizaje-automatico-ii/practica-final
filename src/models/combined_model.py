from sklearn.ensemble import VotingClassifier
from sklearn.pipeline import Pipeline


class CombinedModel:
    def __init__(self, models, models_names):
        if type(models) != list:
            raise ValueError('Variable models must be a list')
        self.models = models
        self.models_names = models_names
        self.classifier = None
        self.X_train, self.X_test, self.y_train, self.y_test = None, None, None, None

    def build_model(self, combination_type):
        if combination_type == "pipeline":
            self.classifier = Pipeline([(model_name, model)
                                        for (model_name, model)
                                        in zip(self.models_names, self.models)])
        elif combination_type == "hard voting":
            self.classifier = VotingClassifier(
                estimators=[(model_name, model)
                            for (model_name, model)
                            in zip(self.models_names, self.models)],
                voting="hard", n_jobs=-1)
        elif combination_type == "soft voting":
            self.classifier = VotingClassifier(
                estimators=[(model_name, model)
                            for (model_name, model)
                            in zip(self.models_names, self.models)],
                voting="soft", n_jobs=-1)
        else:
            raise ValueError('Unknown combination method')
        return self.classifier

