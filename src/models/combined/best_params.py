import json
import os

def read_json_params(_type, model):
    with open(os.path.join(_type, 'data', '{}.json'.format(model))) as json_file:
        data = json.load(json_file)["best_params"]
        return data


rf_param_grid = read_json_params(os.path.join(os.path.abspath(
    os.path.dirname(__file__)), os.pardir, "random_forest"),
    "random_forest")

gbdt_param_grid = read_json_params(os.path.join(os.path.abspath(
    os.path.dirname(__file__)), os.pardir,
    "gradient_boosted_decision_tree"),
    "gradient_boosted_decision_tree")

xgboost_params = read_json_params(os.path.join(os.path.abspath(
    os.path.dirname(__file__)), os.pardir,
    "combined"),
    "xgboost")
mlp_params = read_json_params(os.path.join(os.path.abspath(
    os.path.dirname(__file__)), os.pardir,
    "combined"),
    "mlp_classifier")