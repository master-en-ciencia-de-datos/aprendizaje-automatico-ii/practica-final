from sklearn.cluster import KMeans
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier

pipeline_combinations = {
    "kmeans_random_forest":
        {"models": [KMeans(algorithm="elkan",
                           init="k-means++",
                           max_iter=200,
                           n_clusters=10,
                           n_init=5),
                    RandomForestClassifier(max_depth=50,
                                           min_samples_leaf=10,
                                           n_estimators=200)],

         "models_names": ["kmeans", "random_forest"],
         },

    "kmeans_gbdt":
        {"models": [KMeans(
                        algorithm="full",
                        init="random",
                        max_iter=200,
                        n_clusters=10,
                        n_init=2),
                    GradientBoostingClassifier(
                        learning_rate=0.2,
                        max_depth=5,
                        min_samples_leaf=50,
                        n_estimators=100)],
         "models_names": ["kmeans", "gbdt"]
         },
}
