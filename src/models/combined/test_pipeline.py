"""
Testing the pipeline models. The idea is:
  For each combination:
    1. Read the combination hyperparameters.
    2. Train and test with 10 folds of CVscore
    3. save results in a .json
"""
import json
import os
import numpy as np

from src.models.combined_model import CombinedModel
from src.models.combined.pipeline_best_combination import *
from src.visualization.confussion_matrix import get_acc_f1
from tqdm import tqdm

list_acc_score = []
list_f1_score = [[], [], []]
list_train_time = []

N_ITER = 10
for combination in pipeline_combinations:
    name = "{}".format(combination)
    combination_type = "pipeline"
    for iteracion in tqdm(range(N_ITER), desc=name, leave=False):
        combined_model = CombinedModel(
            models=pipeline_combinations[combination]["models"],
            models_names=pipeline_combinations[combination]["models_names"])
        combined_model_classifier = combined_model.build_model(
            combination_type)

        acc_score, _f1_score, train_time = get_acc_f1(combined_model_classifier,
                                          rm_outlayers=True, normalize=False,
                                          scale=False,)
        list_acc_score.append(acc_score)
        list_f1_score[0].append(_f1_score[0])
        list_f1_score[1].append(_f1_score[1])
        list_f1_score[2].append(_f1_score[2])
        list_train_time.append(train_time)

    json_to_write = {
        "acc_score": np.mean(list_acc_score),
        "f1_score": {
            "micro": np.mean(list_f1_score[0]),
            "macro": np.mean(list_f1_score[1]),
            "weighted": np.mean(list_f1_score[2]),
        },
        "train_time": np.mean(list_train_time)
    }

    with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "data",
                           "results", "{}.json".format(name)), 'w') as outfile:
        json.dump(json_to_write, outfile)
