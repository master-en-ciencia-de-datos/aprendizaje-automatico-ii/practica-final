"""
Hyperparameter tuning for the Custom models. The idea is:
  For both using bootstrap or not:
    1. Search the best hiperparameters iterating over the options of
    get_processed().
    2. Select the best feature engineer technique and follow with it
    3. Plot a confussión matrix over the validation set
"""
import os

from sklearn.neural_network import MLPClassifier
from xgboost import XGBClassifier
import json

from src.models.combined.best_feature_engineer import look_for_best_feature_engineer_options
from src.visualization.confussion_matrix import get_confussion_matrix
from sklearn.cluster import KMeans
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from src.models.params import *
from src.models.combined_model import CombinedModel

CV = 10

pipeline_combinations = {
    "kmeans_random_forest": {"models": [KMeans(), RandomForestClassifier()],
                             "models_names": ["kmeans", "random_forest"],
                             "param_grid": {**kmeans_param_grid,
                                            **rf_param_grid}},

    "kmeans_gbdt": {"models": [KMeans(), GradientBoostingClassifier()],
                    "models_names": ["kmeans", "gbdt"],
                    "param_grid": {**kmeans_param_grid, **gbdt_param_grid}},
}

def plot_for_combination(_combination, dict_combination, _name,
                         _combination_type):
    print("---> " + _name)
    param_grid = dict_combination[_combination]["param_grid"]

    if _combination_type is None:
        combined_model_classifier = dict_combination[_combination]["models"]

    else:
        combined_model = CombinedModel(
            models=dict_combination[_combination]["models"],
            models_names=dict_combination[_combination]["models_names"])
        combined_model_classifier = combined_model.build_model(_combination_type)

    # 1. Best feature engineering
    print("-"*10 + " 1. Looking for best params and feat eng " + "-"*10)
    (rm_outlayers, normalize, scale), best_params = \
        look_for_best_feature_engineer_options(combined_model_classifier,
                                               param_grid, CV)

    # 2. The chosen ones
    print("-"*10 + " 2. The result " + "-"*10)
    print("Remove outlayers: {}\nNormalize: {}\nScale: {}\nBest params: {}"
          .format(rm_outlayers, normalize, scale, best_params))

    # 3. Plot confussion matrix
    print("-"*10 + " 3. Confussion matrix " + "-"*10)
    temp_model = combined_model_classifier.set_params(**best_params)
    acc_score, _f1_score = get_confussion_matrix(temp_model, rm_outlayers,
                                                 normalize, scale,
                                                 random_split=0.2, title=_name,
                                                 model_path=os.path.abspath(
                                                     os.path.dirname(__file__)))

    json_to_write = {
        "rm_outlayers": rm_outlayers,
        "normalize": normalize,
        "scale": scale,
        "best_params": best_params,
        "acc_score": acc_score,
        "f1_score": {
            "micro": _f1_score[0],
            "macro": _f1_score[1],
            "weighted": _f1_score[2],
        }
    }

    with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "data",
                           "{}.json".format(_name)), 'w') as outfile:
        json.dump(json_to_write, outfile)


for combination in pipeline_combinations:
    name = "{} ".format(combination)
    combination_type = "pipeline"
    plot_for_combination(combination, pipeline_combinations, name,
                         combination_type)

# NEW_WAY: GridSearchCV on the separate models, not the combined ones.

voting_models = {
    "mlp_classifier": {"models_names": "MLPClassifier", "models": MLPClassifier(),
                       "param_grid": mlp_params},
    "xgboost": {"models_names": "XGBoost", "models": XGBClassifier(),
                "param_grid": xgboost_params},
}
for model in voting_models:
    name = "{}".format(model)
    plot_for_combination(model, voting_models, name, _combination_type=None)

