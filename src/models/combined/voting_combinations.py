from src.models.combined.best_params import *
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier
from xgboost import XGBClassifier

voting_combinations = {
    "xgboost_random_forest": {"models": [XGBClassifier(**xgboost_params),
                                         RandomForestClassifier(**rf_param_grid)],
                              "models_names": ["xgboost", "random_forest"]},

    "xgboost_gbdt": {"models": [XGBClassifier(**xgboost_params),
                                GradientBoostingClassifier(**gbdt_param_grid)],
                     "models_names": ["xgboost", "gbdt"]},

    "mlp_random_forest": {"models": [MLPClassifier(**mlp_params),
                                     RandomForestClassifier(**rf_param_grid)],
                          "models_names": ["mlp", "random_forest"]},

    "mlp_gbdt": {"models": [MLPClassifier(**mlp_params),
                            GradientBoostingClassifier(**gbdt_param_grid)],
                 "models_names": ["mlp", "gbdt"]},

    "xgboost_mlp_random_forest": {"models": [XGBClassifier(**xgboost_params),
                                             MLPClassifier(**mlp_params),
                                             RandomForestClassifier(**rf_param_grid)],
                                  "models_names": ["xgboost", "mlp", "random_forest"]},

    "xgboost_mlp_gbdt": {"models": [XGBClassifier(**xgboost_params),
                                    MLPClassifier(**mlp_params),
                                    GradientBoostingClassifier(**gbdt_param_grid)],
                         "models_names": ["xgboost", "mlp", "gbdt"]}
}
