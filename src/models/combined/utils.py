import json
import os

from src.visualization.confussion_matrix import get_confussion_matrix


def plot_and_save_voting_results(_model, _name):
    print("---> " + _name)

    acc_score, _f1_score = get_confussion_matrix(_model,
                                                 rm_outlayers=True,
                                                 normalize=False,
                                                 scale=False,
                                                 random_split=0.2, title=_name,
                                                 model_path=os.path.abspath(
                                                     os.path.dirname(__file__)))

    json_to_write = {
        "acc_score": acc_score,
        "f1_score": {
            "micro": _f1_score[0],
            "macro": _f1_score[1],
            "weighted": _f1_score[2],
        }
    }

    with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "data",
                           "results", "{}.json".format(_name)), 'w') as outfile:
        json.dump(json_to_write, outfile)
