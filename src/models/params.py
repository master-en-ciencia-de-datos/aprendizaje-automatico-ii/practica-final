kmeans_param_grid = {"kmeans__n_clusters": [2, 3, 4, 5, 10, 20],
                     'kmeans__init': ['k-means++', 'random'],
                     'kmeans__n_init': [2, 3, 5, 10],
                     'kmeans__max_iter': [10, 50, 200, 500],
                     'kmeans__algorithm': ['full', 'elkan']
                     }

rf_param_grid = {'random_forest__max_depth': [1, 2, 5, 10, 20, 50],
                 'random_forest__min_samples_leaf': [1, 10, 50, 100],
                 'random_forest__n_estimators': [5, 10, 50, 100, 200]
                 }

rf_params = {'max_depth': [1, 2, 5, 10, 20, 50],
             'min_samples_leaf': [1, 10, 50, 100],
             'n_estimators': [5, 10, 50, 100, 200]
             }

gbdt_param_grid = {'gbdt__max_depth': [1, 5, 10],
                   'gbdt__min_samples_leaf': [1, 10, 50],
                   'gbdt__n_estimators': [10, 100, 200],
                   'gbdt__learning_rate': [0.2, 0.4, 0.6, 0.8, 1.0]
                   }

gbdt_params = {'max_depth': [1, 5, 10],
               'min_samples_leaf': [1, 10, 50],
               'n_estimators': [10, 100, 200],
               'learning_rate': [0.2, 0.4, 0.6, 0.8, 1.0]
               }

xgboost_params = {
    'min_child_weight': [0, 0.5, 1, 5, 10],
    'gamma': [0, 0.5, 1, 1.5, 2, 5],
    'subsample': [0.2, 0.4, 0.6, 0.8, 1.0],
    'colsample_bytree': [0.2, 0.4, 0.6, 0.8, 1.0],
    'max_depth': [1, 2, 5, 10, 20]
}

mlp_params = {
    'hidden_layer_sizes': [(5, 5, 5), (50, 50, 50), (50, 100, 50), (100,)],
    'activation': ['tanh', 'relu'],
    'solver': ['sgd', 'adam'],
    'alpha': [0.0001, 0.05],
    'learning_rate': ['constant', 'adaptive'],
}