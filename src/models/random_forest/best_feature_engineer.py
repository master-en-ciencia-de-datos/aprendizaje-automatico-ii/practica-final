from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
import numpy as np
from tqdm import tqdm

from src.data.make_dataset import get_X_y, get_processed

BOOTSTRAP_OPTIONS = [True, False]
FEATURE_ENGINEERING_OPTIONS = {"rm_outlayers": [True, False],
                               "normalize": [True, False],
                               "scale": [True, False]
                               }


def look_for_best_feature_engineer_options(rf_param_grid, cv):
    scores = []
    options = []
    params = []
    pbar = tqdm(total=16)
    for bootstrap in BOOTSTRAP_OPTIONS:
        for rm_outlayers in FEATURE_ENGINEERING_OPTIONS["rm_outlayers"]:
            for normalize in FEATURE_ENGINEERING_OPTIONS["normalize"]:
                for scale in FEATURE_ENGINEERING_OPTIONS["normalize"]:
                    df = get_processed(rm_outlayers, normalize, scale,
                                       dumies=["neighbourhood",
                                               "neighbourhood_group"],
                                       drop_columns=None)

                    X_train, _, y_train, _ = get_X_y(df, random_split=0)
                    random_forest = RandomForestClassifier(bootstrap=bootstrap)
                    random_forest_grid_search = GridSearchCV(
                        estimator=random_forest,
                        param_grid=rf_param_grid,
                        cv=cv, n_jobs=3, verbose=1)

                    random_forest_grid_search.fit(X_train, y_train)

                    options.append((bootstrap, rm_outlayers, normalize, scale))
                    scores.append(random_forest_grid_search.best_score_)
                    params.append(random_forest_grid_search.best_params_)
                    pbar.update(1)
    pbar.close()

    return options[np.argmax(scores)], params[np.argmax(scores)]
