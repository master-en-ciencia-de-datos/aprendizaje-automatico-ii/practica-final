"""
Testing the RF and Extra Trees model. The idea is:
    1. Train and test with 10 folds of CVscore
    2. save results in a .json
"""
import json
import os
import numpy as np

from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier

from src.visualization.confussion_matrix import get_acc_f1
from tqdm import tqdm

list_acc_score = []
list_f1_score = [[], [], []]
list_train_time = []
N_ITER = 10

models = [RandomForestClassifier(max_depth=20, min_samples_leaf=1,
                                 n_estimators=200, bootstrap=True),
          ExtraTreesClassifier(max_depth=20, min_samples_leaf=1,
                               n_estimators=50, bootstrap=True)]
models_names = ["Random Forest", "Extra trees"]
for (model, name) in zip(models, models_names):
    for iteracion in tqdm(range(N_ITER), desc=name, leave=False):
        acc_score, _f1_score, train_time = get_acc_f1(model,
                                                      rm_outlayers=True,
                                                      normalize=False,
                                                      scale=False, )
        list_acc_score.append(acc_score)
        list_f1_score[0].append(_f1_score[0])
        list_f1_score[1].append(_f1_score[1])
        list_f1_score[2].append(_f1_score[2])
        list_train_time.append(train_time)
    json_to_write = {
        "acc_score": np.mean(list_acc_score),
        "f1_score": {
            "micro": np.mean(list_f1_score[0]),
            "macro": np.mean(list_f1_score[1]),
            "weighted": np.mean(list_f1_score[2]),
        },
        "train_time": np.mean(list_train_time)
    }

    with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "data",
                           "results", "{}.json".format(name)), 'w') as outfile:
        json.dump(json_to_write, outfile)
