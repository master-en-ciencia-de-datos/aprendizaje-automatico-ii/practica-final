import numpy as np
import matplotlib.pyplot as plt


def plot_grid_search(cv_results, grid_param_1, grid_param_2, name_param_1,
                     name_param_2):
    # Get Test Scores Mean and std for each grid search
    scores_mean = cv_results['mean_test_score']
    scores_mean = np.array(scores_mean).reshape(len(grid_param_2),
                                                len(grid_param_1))

    scores_sd = cv_results['std_test_score']
    scores_sd = np.array(scores_sd).reshape(len(grid_param_2),
                                            len(grid_param_1))

    # Plot Grid search scores
    _, ax = plt.subplots(1, 1)

    # Param1 is the X-axis, Param 2 is represented as a different curve (color line)
    for idx, val in enumerate(grid_param_2):
        ax.plot(grid_param_1, scores_mean[idx, :], '-o',
                label=name_param_2 + ': ' + str(val))
        ax.fill_between(grid_param_1, scores_mean[idx, :] - scores_sd[idx, :],
                        scores_mean[idx, :] + scores_sd[idx, :],
                        alpha=0.2,
                        # edgecolor='sandybrown',
                        # facecolor='lightsalmon',
                        # linewidth=4, linestyle='dashdot', antialiased=True
                        )

    ax.set_title("Puntuación del Grid Search", fontsize=20, fontweight='bold')
    ax.set_xlabel(name_param_1, fontsize=16)
    ax.set_ylabel('Puntuación media del CV', fontsize=16)
    ax.legend(loc="best", fontsize=15)
    ax.grid('on')


def plot_heatmap(cv_results, grid_param_1, grid_param_2, name_param_1,
                 name_param_2):
    # Get Test Scores Mean and std for each grid search
    scores_mean = cv_results['mean_test_score']
    scores_mean = np.array(scores_mean).reshape(len(grid_param_2),
                                                len(grid_param_1))

    # Plot Grid search scores
    fig, ax = plt.subplots()

    X, Y = np.meshgrid(grid_param_1, grid_param_2)

    Z = scores_mean.reshape(len(grid_param_2), len(grid_param_1))

    im = ax.imshow(Z)
    # We want to show all ticks...
    ax.set_xticks(np.arange(len(grid_param_1)))
    ax.set_yticks(np.arange(len(grid_param_2)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(grid_param_1)
    ax.set_yticklabels(grid_param_2)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    for i in range(len(grid_param_2)):
        for j in range(len(grid_param_1)):
            text = ax.text(j, i, "{:.2f}".format(Z[i, j]),
                           ha="center", va="center", color="w")

    ax.set_title("Puntuación del Grid Search", fontsize=20, fontweight='bold')
    ax.set_xlabel(name_param_1, fontsize=16)
    ax.set_ylabel(name_param_2, fontsize=16)
    fig.tight_layout()
