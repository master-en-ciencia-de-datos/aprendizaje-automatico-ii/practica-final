import matplotlib.pyplot as plt
import numpy as np


def plot_mean_std(mean_train_score, mean_val_score, std_train_score,
                  std_val_score, plotinfo, ax=None):
    mean_train_score = np.array(mean_train_score)
    mean_val_score = np.array(mean_val_score)
    std_train_score = np.array(std_train_score)
    std_val_score = np.array(std_val_score)
    # Plot Grid search scores
    if ax is None:
        fig, ax = plt.subplots(1, 1)

    ax.plot(plotinfo["xvals"], mean_train_score, '-o',
            label="train")
    ax.fill_between(plotinfo["xvals"], mean_train_score - std_train_score,
                    mean_train_score + std_train_score,
                    alpha=0.2,
                    linewidth=4, linestyle='dashdot', antialiased=True
                    )
    ax.plot(plotinfo["xvals"], mean_val_score, '-o',
            label="val")
    ax.fill_between(plotinfo["xvals"], mean_val_score - std_val_score,
                    mean_val_score + std_val_score,
                    alpha=0.2,
                    linewidth=4, linestyle='dashdot', antialiased=True
                    )

    ax.set_title(plotinfo["title"], fontsize=18, fontweight='bold')
    ax.set_xlabel(plotinfo["xlabel"], fontsize=17)
    ax.set_ylabel('Puntuación media del CV', fontsize=17)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(14)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(14)
    ax.legend(loc="best", fontsize=16)
    ax.grid('on')
    return ax
