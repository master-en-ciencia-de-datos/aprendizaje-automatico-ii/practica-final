import os
import time

from sklearn import model_selection
from sklearn.metrics import confusion_matrix, make_scorer
import matplotlib.pyplot as plt
import numpy as np

from sklearn.metrics import accuracy_score, f1_score
from src.data.make_dataset import get_processed, get_X_y


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues,
                          fig=None, ax=None):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    #classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        #print("Normalized confusion matrix")
    else:
        pass
        #print('Confusion matrix, without normalization')

    #print(cm)

    if fig is None and ax is None:

        fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


def get_confussion_matrix(model_classifier, rm_outlayers, normalize,
                          scale, random_split=0.2, title="Matriz de confusion",
                          train_score=False, model_path=None):
    df = get_processed(rm_outlayers, normalize, scale,
                       dumies=["neighbourhood",
                               "neighbourhood_group"],
                       drop_columns=None)
    X_train, X_test, y_train, y_test = get_X_y(df, random_split,
                                               random_state=42)
    model_classifier.fit(X_train, y_train)
    y_pred = model_classifier.predict(X_test)

    acc_score = accuracy_score(y_test, y_pred) * 100
    micro_f1_score = f1_score(y_test, y_pred, average="micro")
    macro_f1_score = f1_score(y_test, y_pred, average="macro")
    weighted_f1_score = f1_score(y_test, y_pred, average="weighted")

    _f1_score = [micro_f1_score, macro_f1_score, weighted_f1_score]
    fig, ax = plt.subplots(1, 1)
    ax = plot_confusion_matrix(y_test, y_pred,
                               classes=["Entire room", "Private room",
                                        "Shared room"],
                               normalize=False,
                               title="{}: {:.2f}%".format(title, acc_score),
                               cmap=plt.cm.Blues,fig=fig, ax=ax)

    if train_score:
        fig2, ax2 = plt.subplots(1, 1)
        ax2.plot(model_classifier.train_score_,
                 label="loss {}".format(model_classifier.loss))
        ax2.set_xlabel("Iteración")
        ax2.set_ylabel("Error de entrenamiento")
        ax2.set_title("Error según el número de iteraciones")
        ax2.legend()
        ax2.grid('on')
        fig2.savefig(os.path.join(model_path, "images", title + "_train_score"))

    fig.savefig(os.path.join(model_path, "images", title))
    return acc_score, _f1_score


def get_acc_f1(model_classifier, rm_outlayers, normalize,
               scale):
    df = get_processed(rm_outlayers, normalize, scale,
                       dumies=["neighbourhood",
                               "neighbourhood_group"],
                       drop_columns=None)
    X_train, X_test, y_train, y_test = get_X_y(df, random_split=0.2)
    start_time = time.time()
    model_classifier.fit(X_train, y_train)
    train_time = time.time() - start_time
    y_pred = model_classifier.predict(X_test)

    acc_score = accuracy_score(y_test, y_pred)
    micro_f1_score = f1_score(y_test, y_pred, average="micro")
    macro_f1_score = f1_score(y_test, y_pred, average="macro")
    weighted_f1_score = f1_score(y_test, y_pred, average="weighted")

    _f1_score = [micro_f1_score, macro_f1_score, weighted_f1_score]

    return acc_score, _f1_score, train_time
