import os

from src.visualization.boxplot.utils import gather_data, TRANSLATION_DICT
import matplotlib.pyplot as plt

results = gather_data()

metrics = ["acc_score", "f1_score_macro", "f1_score_weighted"]

fig, axes = plt.subplots(1, 3, figsize=(27, 9))

for (metric, ax) in zip(metrics, fig.axes):
    names_all_together = [model for model in results]
    xticks_labels = [TRANSLATION_DICT[model] for model in results]
    score_all_together = []
    for model in names_all_together:
        score_all_together.append(results[model][metric])

    list_ordered = [1, 2, 3, 14, 6, 8, 4, 17, 9, 5, 7, 15, 13, 16, 11, 10, 12]
    list_ordered, xticks_labels, score_all_together = (list(t) for t in zip(
        *sorted(zip(list_ordered, xticks_labels, score_all_together))))

    # Create the boxplot
    bp = ax.boxplot(score_all_together)
    ax.set_xticklabels(xticks_labels, fontsize=12, rotation=90)

    if metric == "acc_score":
        ax.set_title("Precisión")
    elif metric == "f1_score_macro":
        ax.set_title("F1 macro")
    elif metric == "f1_score_weighted":
        ax.set_title("F1 ponderado")
    ymin, ymax = ax.get_ylim()[0], ax.get_ylim()[1]
    for x_position in [3.5, 5.5, 11.5]:
        ax.axvline(x_position, ymin, ymax, linestyle="dashed", color="royalblue")
    for (x_position, text) in zip(
            [1, 3.9, 7.5, 13.5],
            ["modelo base", "pipeline", "voto ponderado", "voto por mayoría"]):
        ax.text(x_position, ymin, text)

fig.tight_layout()
plt.subplots_adjust(top=0.95)
fig.savefig(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                         "images", "all"))

plt.show()