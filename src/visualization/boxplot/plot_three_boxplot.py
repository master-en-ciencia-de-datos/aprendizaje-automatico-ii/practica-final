import os

from src.visualization.boxplot.utils import gather_data, TRANSLATION_DICT
import matplotlib.pyplot as plt

results = gather_data()

metrics = ["acc_score", "f1_score_macro", "f1_score_weighted"]
for metric in metrics:
    names_all_together = [model for model in results]
    xticks_labels = [TRANSLATION_DICT[model] for model in results]
    score_all_together = []
    for model in names_all_together:
        score_all_together.append(results[model][metric])

    list_ordered = [1, 2, 3, 14, 6, 8, 4, 17, 9, 5, 7, 15, 13, 16, 11, 10, 12]
    list_ordered, xticks_labels, score_all_together = (list(t) for t in zip(
        *sorted(zip(list_ordered, xticks_labels, score_all_together))))

    # Create a figure instance
    fig = plt.figure(1, figsize=(9, 9))

    # Create an axes instance
    ax = fig.add_subplot(111)

    # Create the boxplot
    bp = ax.boxplot(score_all_together)
    ax.set_xticklabels(xticks_labels, fontsize=16, rotation=90)
    ax.tick_params(axis='y', labelsize=14)

    if metric == "acc_score":
        fig.suptitle("Precisión", fontsize=20)
    elif metric == "f1_score_macro":
        fig.suptitle("F1 macro", fontsize=20)
    elif metric == "f1_score_weighted":
        fig.suptitle("F1 ponderado", fontsize=20)
    ymin, ymax = ax.get_ylim()[0], ax.get_ylim()[1]
    for x_position in [3.5, 5.5, 11.5]:
        plt.vlines(x_position, ymin, ymax, linestyles="dashed", colors="royalblue")
    for (x_position, text) in zip(
            [0.9, 3.75, 7.0, 13.1],
            ["modelo base", "pipeline", "voto ponderado", "voto por mayoría"]):
        plt.text(x_position, ymin, text, fontsize=13)

    fig.tight_layout()
    plt.subplots_adjust(top=0.95)
    fig.savefig(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                             "images", metric))

    plt.clf()
plt.show()