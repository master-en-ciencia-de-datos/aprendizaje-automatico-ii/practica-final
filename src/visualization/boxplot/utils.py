import json
import os


def gather_data():
    scores = {}
    for folder in ["random_forest", "gradient_boosted_decision_tree",
                   "combined"]:
        results_folder_path = os.path.join(
            os.path.abspath(os.path.dirname(__file__)),
            os.pardir, os.pardir, "models", folder, "data", "results")
        for json_file in os.listdir(results_folder_path):
            if json_file == ".gitkeep":
                continue

            name = json_file.split(".json")[0]
            with open(os.path.join(results_folder_path, json_file), 'r') as f:
                data = json.loads(f.read())

            acc_score = data["acc_score"]
            f1_score_macro = data["f1_score"]["macro"]
            f1_score_weighted = data["f1_score"]["weighted"]

            scores[name] = {
                "acc_score": acc_score,
                "f1_score_macro": f1_score_macro,
                "f1_score_weighted": f1_score_weighted,
            }

    return scores


TRANSLATION_DICT = {
    "Extra trees": "ET",
    "Random Forest": "RF",
    "gbdt": "GBDT",
    "mlp_random_forest voting hard": "MLP+RF(hard)",
    "xgboost_random_forest voting soft": "XGB+RF(soft)",
    "mlp_random_forest voting soft": "MLP+RF(soft)",
    "kmeans_random_forest": "KM+RF",
    "xgboost_mlp_gbdt voting hard": "XGB+MLP+GBDT(hard)",
    "mlp_gbdt voting soft": "MLP+GBDT(soft)",
    "kmeans_gbdt": "KM+GBDT",
    "xgboost_gbdt voting soft": "XGB+GBDT(soft)",
    "xgboost_mlp_random_forest voting hard": "XGB+MLP+RF(hard)",
    "xgboost_gbdt voting hard": "XGB+GBDT(hard)",
    "mlp_gbdt voting hard": "MLP+GBDT(hard)",
    "xgboost_mlp_gbdt voting soft": "XGB+MLP+GBDT(soft)",
    "xgboost_mlp_random_forest voting soft": "XGB+MLP+RF(soft)",
    "xgboost_random_forest voting hard": "XGB+RF(hard)",
}



