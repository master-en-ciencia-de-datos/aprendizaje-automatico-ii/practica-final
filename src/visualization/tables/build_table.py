from src.visualization.tables.calc_param import get_times, TRANSLATION_DICT, \
    calc_params
import pandas as pd

times = get_times()
params = calc_params()

list_models = list(times.keys())
list_times = list(times.values())
list_params = list(params.values())

data = {
    "modelo": list_models,
    "tiempo de entrenamiento": list_times,
    "número de parámetros": list_params,
}
df = pd.DataFrame(data)
df["modelo"] = df["modelo"].map(TRANSLATION_DICT)

list_ordered = [0, 1, 2, 13, 5, 7, 3, 16, 8, 4, 6, 15, 12, 14, 10, 9, 11]
df = df.reindex(list_ordered)
print(df)
df = df.sort_index()
df.to_csv("table.csv", index=False)
