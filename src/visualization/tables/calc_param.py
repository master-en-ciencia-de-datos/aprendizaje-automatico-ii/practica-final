import json
import os
import numpy as np

from src.models.params import *


def get_times():
    times = {}
    for folder in ["random_forest", "gradient_boosted_decision_tree",
                   "combined"]:
        results_folder_path = os.path.join(
            os.path.abspath(os.path.dirname(__file__)),
            os.pardir, os.pardir, "models", folder, "data", "results")
        for json_file in os.listdir(results_folder_path):
            if json_file == ".gitkeep":
                continue

            name = json_file.split(".json")[0]
            with open(os.path.join(results_folder_path, json_file), 'r') as f:
                data = json.loads(f.read())

            train_time = data["train_time"]

            times[name] = np.mean(train_time)

    return times


TRANSLATION_DICT = {
    "Extra trees": "ET",
    "Random Forest": "RF",
    "gbdt": "GBDT",
    "mlp_random_forest voting hard": "MLP+RF(hard)",
    "xgboost_random_forest voting soft": "XGB+RF(soft)",
    "mlp_random_forest voting soft": "MLP+RF(soft)",
    "kmeans_random_forest": "KM+RF",
    "xgboost_mlp_gbdt voting hard": "XGB+MLP+GBDT(hard)",
    "mlp_gbdt voting soft": "MLP+GBDT(soft)",
    "kmeans_gbdt": "KM+GBDT",
    "xgboost_gbdt voting soft": "XGB+GBDT(soft)",
    "xgboost_mlp_random_forest voting hard": "XGB+MLP+RF(hard)",
    "xgboost_gbdt voting hard": "XGB+GBDT(hard)",
    "mlp_gbdt voting hard": "MLP+GBDT(hard)",
    "xgboost_mlp_gbdt voting soft": "XGB+MLP+GBDT(soft)",
    "xgboost_mlp_random_forest voting soft": "XGB+MLP+RF(soft)",
    "xgboost_random_forest voting hard": "XGB+RF(hard)",
}

models_params = {
    "Extra trees": [rf_param_grid],
    "Random Forest": [rf_param_grid],
    "gbdt": [gbdt_param_grid],
    "mlp_random_forest voting hard": [mlp_params, rf_param_grid],
    "xgboost_random_forest voting soft": [xgboost_params, rf_param_grid],
    "mlp_random_forest voting soft": [mlp_params, rf_param_grid],
    "kmeans_random_forest": [kmeans_param_grid, rf_param_grid],
    "xgboost_mlp_gbdt voting hard": [xgboost_params, gbdt_param_grid],
    "mlp_gbdt voting soft": [mlp_params, gbdt_param_grid],
    "kmeans_gbdt": [kmeans_param_grid, gbdt_param_grid],
    "xgboost_gbdt voting soft": [xgboost_params, gbdt_param_grid],
    "xgboost_mlp_random_forest voting hard": [xgboost_params, mlp_params, rf_param_grid],
    "xgboost_gbdt voting hard": [xgboost_params, gbdt_param_grid],
    "mlp_gbdt voting hard": [mlp_params, gbdt_param_grid],
    "xgboost_mlp_gbdt voting soft": [xgboost_params, mlp_params, gbdt_param_grid],
    "xgboost_mlp_random_forest voting soft": [xgboost_params, mlp_params, rf_param_grid],
    "xgboost_random_forest voting hard": [xgboost_params, rf_param_grid],
}


def calc_params():
    params = {}
    for model in models_params:
        lengths = []
        for param_grid in models_params[model]:
            for param_name in param_grid:
                lengths.append(len(param_grid[param_name]))

        params[model] = np.prod(np.array(lengths))

    return params


