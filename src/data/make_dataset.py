import copy

import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
from sklearn.model_selection import train_test_split

from src.features.build_features import *
import os

my_path = os.path.abspath(os.path.dirname(__file__))
RAW_CSV_PATH = os.path.join(my_path, "../../data/raw/airbnb.csv")


def get_raw():
    df = pd.read_csv(RAW_CSV_PATH)
    return df


def get_processed(rm_outlayers=False, normalize=False, scale=False,
                  dumies=None, drop_columns=None):
    df = get_raw()
    if rm_outlayers:
        df = remove_outlayers(df)
    if normalize:
        df = log_scale(df)
        df.replace(-np.inf, 0, inplace=True)
    if scale:
        df = min_max_scale(df)
    if dumies is not None:
        if type(dumies) != list:
            df = get_dumies(df, None)
        else:
            df = get_dumies(df, dumies)
    if drop_columns is not None:
        for column in drop_columns:
            df = df.drop(column, axis=1)
    return df


def get_X_y(df, random_split=0.2, random_state=None):
    if df is None:
        df = get_processed(rm_outlayers=False, normalize=False, scale=False,
                           dumies=["neighbourhood", "neighbourhood_group"],
                           drop_columns=None)
    temp_df = copy.copy(df)
    y = temp_df.pop("room_type")
    X = temp_df
    if random_split == 0:
        return X, None, y, None
    else:
        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                            random_state=random_state,
                                                            test_size=random_split)
        return X_train, X_test, y_train, y_test
