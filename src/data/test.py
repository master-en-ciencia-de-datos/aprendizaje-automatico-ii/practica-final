from src.data.make_dataset import *

print(len(get_raw()))
print(len(get_processed(rm_outlayers=False, normalize=False, scale=False,
                        dumies=False)))
print(len(get_processed(rm_outlayers=True, normalize=False, scale=False,
                        dumies=False)))
print(len(get_processed(rm_outlayers=True, normalize=True, scale=False,
                        dumies=False)))
print(len(get_processed(rm_outlayers=True, normalize=True, scale=True,
                        dumies=False)))
print(len(get_processed(rm_outlayers=True, normalize=True, scale=True,
                        dumies=True)))
