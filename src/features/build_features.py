import numpy as np
np.seterr(divide='ignore')  # Ignore dividing by 0 in the log scale
from sklearn.preprocessing import MinMaxScaler
import pandas as pd


def remove_outlayers(df):
    df = df.loc[df['minimum_nights'] <= 10]
    df = df.loc[df['price'] <= 200]
    return df


def log_scale(df):
    columns = ["price", "number_of_reviews", "reviews_per_month",
               "calculated_host_listings_count"]
    for column in columns:
        df[column] = np.log(df[column])
    return df


def min_max_scale(df):
    columns = ["price", "number_of_reviews", "reviews_per_month",
               "calculated_host_listings_count", "longitude", "latitude",
               "minimum_nights"]
    for column in columns:
        scaler = MinMaxScaler()
        df[column] = scaler.fit_transform(df[column].values.reshape(-1, 1))
    return df


def get_dumies(df, columns=None):
    if columns is None:
        columns = ["neighbourhood", "neighbourhood_group", "room_type"]
    elif type(columns) == str:
        columns = [columns]
    elif type(columns) == list:
        for column in columns:
            df = pd.get_dummies(df, columns=[column],
                                drop_first=True)
    else:
        print("Unknown type for columns")

    return df
