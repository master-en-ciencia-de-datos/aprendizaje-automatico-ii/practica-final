#echo "Starting the full study"
#
#echo "---------------------------------"
#echo "  --> Running random_forest <--  "
#
#python3 -m src.models.random_forest.train_model
#
#echo "------------------------"
#echo "  --> Running GBDT <--  "
#
#python3 -m src.models.gradient_boosted_decision_tree.train_model

echo "-----------------------------------"
echo "  --> Running combined models <--  "

python3 -m src.models.combined.train_model

echo "--------------------"
echo "  --> Finished <--  "